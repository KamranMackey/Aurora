# Taliyah (Bot) GitHub Repository

This is the official source code repository on GitHub for the Telegram bot Taliyah. This repository contains the
entire source code, various files such as the README and License files, and specific files needed to install Taliyah's
dependencies.

**Note**: Taliyah is still in development, and because of this, it may (and can!) contain quite a few bugs and not
everything is properly implemented yet, and there are a lot of things I want to implement into Taliyah that haven't
been implemented.

## Libraries Used

* [python-telegram-bot](https://github.com/python-telegram-bot/python-telegram-bot) - Core library used to interface with the Telegram Bot API.
* [requests](https://github.com/psf/requests) - Used for various plugins (such as `xkcd`) to communicate with various web APIs.
* [psutil](https://github.com/giampaolo/psutil) - Used in `system.py` to retrieve various system details like CPU usage.

## Installation

Python 3.8 or later is required to run Taliyah. It is recommended however that you use Python 3.10 or later (Python 3.11 is best) to run
Taliyah as later versions of Python have major improvements to performance, especially Python 3.11.

If you try running Taliyah with an older version of Python, Taliyah will alert you that she cannot be run on your given Python version.

> *Note*: Python 3.12 is not currently supported! It is currently in alpha, and no major Python libraries currently have support for it.

### Clone the repository

```none
git clone https://github.com/evelynmarie/Taliyah.git
cd Taliyah
```

### Install required dependencies

```none
pip install -r requirements.txt
```

### Mark as excutable (Linux-only!)

```none
chmod +x Taliyah.py

```

Or, if you don't want to mess with file permissions, you can also just run the main script via the Python executable:

```none
python Taliyah.py
```

### Run

Run `Taliyah.py` once so that it can generate the `config.ini` file. Then, edit `config.ini`, and under the `Basic Settings` section, set your token to the one you received from BotFather. Once you're done doing that, try running the bot again. If successful, you should have a bit of jargon sent to your terminal's stdout, like shown in the example below:

```none
DATE/TIME - Taliyah - INFO - Initializing the config system.
DATE/TIME - Taliyah - INFO - Config file found. Not overwriting existing file.
DATE/TIME - Taliyah - INFO - Config system initialized. Loading the bot.
DATE/TIME - Taliyah - INFO - Starting plugin loading sequence.
DATE/TIME - Taliyah - INFO - Plugin loading sequence complete. All plugins loaded.
```

## License

```none
Copyright © 2016-2022 Evelyn Marie and contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with This program. If not, see <http://www.gnu.org/licenses/>.
```
