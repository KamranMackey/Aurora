#
# Copyright (C) 2016-2022 Evelyn Marie and contributors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

import logging
import platform
import re
import subprocess

import psutil
from telegram.constants import ChatAction
from uptime import uptime as get_uptime

if platform.system() == "Windows":
    import winreg

logger = logging.getLogger("Taliyah")


def seconds_to_str(seconds):
    """Converts seconds to a string."""
    days, remainder = divmod(seconds, 86400)
    hours, remainder = divmod(remainder, 3600)
    minutes, secs = divmod(remainder, 60)
    return f"Up for {days} day(s), {hours:02}:{minutes:02}:{secs:02} hours"


async def system(update, _) -> None:
    """Retrieve information about the host system."""

    os = platform.system()
    version = platform.release()
    python = platform.python_version()

    def windows_detection_build():
        """
        If Windows is detected, add "build" to the OS build string. If Windows is
        not detected, just print the platform.version variable.
        """
        if platform.system() == "Windows" | platform.system() == "win32":
            return f"build {platform.version()}"
        return platform.version()

    build = windows_detection_build()

    def retrieve_cpu_name():
        """Retrieve the CPU name of the host system."""
        if platform.system() == "Windows" | platform.system() == "win32":
            try:
                key = winreg.OpenKey(winreg.HKEY_LOCAL_MACHINE, "Hardware\\Description\\System\\CentralProcessor\\0")
                processor_brand = winreg.QueryValueEx(key, "ProcessorNameString")[0]
                winreg.CloseKey(key)
                return processor_brand
            except FileNotFoundError:
                logger.error("The specified registry key could not be located in the registry.")
                update.message.reply_text(text="Sorry, the system command occurred an error. Try again later!")
                return None
        elif platform.system() == "Darwin":
            os.environ["PATH"] = os.environ["PATH"] + os.pathsep + "/usr/sbin"
            return subprocess.check_output("sysctl -n machdep.cpu.brand_string").strip()
        elif platform.system() == "Linux":
            output = subprocess.check_output("cat /proc/cpuinfo", shell=True).strip()
            for line in output.decode("utf-8").split("\n"):
                if "model name" in line:
                    return re.sub(".*model name.*:", "", line, 1)
        return ""

    try:
        cpu_name = "*CPU:* ", retrieve_cpu_name()
        cpu_name_2 = "".join(c for c in cpu_name if c.lower() not in "(" ")")
    except AttributeError:
        logger.error("Could not retrieve CPU name.")
        return

    uptime = f"*Uptime:* {seconds_to_str(int(get_uptime()))}"

    cpu_count = psutil.cpu_count(logical=False)
    cpu_threads = psutil.cpu_count(logical=True)
    cpu_string = str(psutil.cpu_percent(interval=0.5, percpu=False)).replace("[", " ").replace("]", " ")

    cpu_cores = f"*CPU cores:* {cpu_count} cores, {cpu_threads} threads"
    cpu_load = f"*CPU usage:* {cpu_string}%"

    await update.message.chat.send_action(action=ChatAction.TYPING)
    await update.message.reply_markdown(f"*OS:* {os} {version} {build}\n*Python:* {python}\n{cpu_name_2}\n{cpu_cores}\n{cpu_load}\n{uptime}\n")
